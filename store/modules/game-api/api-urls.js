const BASE_URL = '/v1'

export default {
    GET_FIRST_DATA: `${BASE_URL}/game-data/init`, //get
    GATHERING_MONEY: `${BASE_URL}/user-stat/money-plus`, //put
    OPEN_WOOD_BOX: `${BASE_URL}/open-box/wood`, //post
    OPEN_GOOD_BOX: `${BASE_URL}/open-box/good`, //post
    GAME_RESET:`${BASE_URL}/game-data/reset`, //del
}
