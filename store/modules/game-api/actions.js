import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.GET_FIRST_DATA]: (store) => {
        return axios.get(apiUrls.GET_FIRST_DATA)
    },
    [Constants.GATHERING_MONEY]: (store) => {
        return axios.put(apiUrls.GATHERING_MONEY)
    },
    [Constants.OPEN_WOOD_BOX]: (store) => {
        return axios.post(apiUrls.OPEN_WOOD_BOX)
    },
    [Constants.OPEN_GOOD_BOX]: (store) => {
        return axios.post(apiUrls.OPEN_GOOD_BOX)
    },
    [Constants.GAME_RESET]: (store) => {
        return axios.delete(apiUrls.GAME_RESET)
    },
}
