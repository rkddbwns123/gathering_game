export default {
    GET_FIRST_DATA: 'game-api/getFirstData',
    GATHERING_MONEY: 'game-api/gatheringMoney',
    OPEN_WOOD_BOX: 'game-api/openWoodBox',
    OPEN_GOOD_BOX: 'game-api/openGoodBox',
    GAME_RESET:'game-api/gameReset',
}
