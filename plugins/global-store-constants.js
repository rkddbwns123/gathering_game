import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import getFirstDataConstants from '~/store/modules/game-api/constants'
Vue.prototype.$getFirstDataConstants = getFirstDataConstants

import gatheringMoneyConstants from '~/store/modules/game-api/constants'
Vue.prototype.$gatheringMoneyConstants = gatheringMoneyConstants

import openWoodBoxConstants from '~/store/modules/game-api/constants'
Vue.prototype.$openWoodBoxConstants = openWoodBoxConstants

import openGoodBoxConstants from '~/store/modules/game-api/constants'
Vue.prototype.$openGoodBoxConstants = openGoodBoxConstants

import gameResetConstants from '~/store/modules/game-api/constants'
Vue.prototype.$gameResetConstants = gameResetConstants
